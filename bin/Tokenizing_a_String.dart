import 'dart:io'; 
void main(List<String> arguments) {
  print("input mathematical expression :");
  String txtMat = stdin.readLineSync()!;
  var listInfix = token(txtMat);
  print("Infix :");
  print(listInfix);
  var listPostfix = postfix(listInfix);
  print("Postfix :");
  print(listPostfix);
  var Evaliate = evluatePostfix(listPostfix);
  print("Evluate Postfix :");
  print(Evaliate);
}


List token(String txtMat){
  String txt = ""; 
  var list = [];
  for (int i = 0; i < txtMat.length; i++) {
    if(!isnum(txtMat[i])){ 
      if(txtMat[i] == " "){
        if(txt != ""){
          list.add(txt);
          txt = "";
        }
      }else{
        if(txt != ""){
          list.add(txt);
          txt = "";
        }
        list.add(txtMat[i]);
      }
    }else{
      txt = txt+txtMat[i];
      if(i == txtMat.length-1){
        list.add(txt);
      }
    }
  }
  return list;
}


bool isnum(String x){
  for (int i = 0; i < 10; i++) {
    if(x == i.toString()){
      return true;
    }
  }
  return false;
}


List postfix(List infix){
  var operators = [];
  var postfix = [];
  for (int i = 0; i < infix.length; i++) {
    if(isInteger(infix[i])){
      postfix.add(infix[i]);
    }
    if(isop(infix[i])){
      while(!operators.isEmpty && Prec(infix[i]) < Prec(operators[operators.length-1])){
        postfix.add(operators[operators.length-1]);
        operators.removeAt(operators.length-1);
      }
      operators.add(infix[i]);
    }
    if(infix[i]=="("){
      operators.add(infix[i]);
    }
    if(infix[i]==")"){
      while(operators[operators.length-1] != "("){
        postfix.add(operators[operators.length-1]);
        operators.removeAt(operators.length-1);
      }
      operators.removeAt(operators.length-1);
    }
  }
  while(!operators.isEmpty){
    postfix.add(operators[operators.length-1]);
    operators.removeAt(operators.length-1);
  }
 return postfix;
 
}



bool isInteger(String x){
  for (int i = 0; i < 10; i++) {
    if(x[0] == i.toString()){
      return true;
    }
  }
  return false;
}

bool isop(String x){
    if(x[0] == "+" ||x[0] == "-" ||x[0] == "*" ||x[0] == "/" ||x[0] == "^"){
      return true;
    }
  return false;
}

int Prec(String x){
  switch (x){
    case '+':
    case '-':
      return 1;
    case '*':
    case '/':
      return 2;
    case '^':
      return 3;
  }
  return -1;
}

List evluatePostfix(List listPostfix){
  var values = [];
  for (int i = 0; i < listPostfix.length; i++) {
    if(isInteger(listPostfix[i])){
      values.add(listPostfix[i]);
    }else{
      int num1 = int.parse(values[values.length-2]);
      int num2 = int.parse(values[values.length-1]);
      values.removeAt(values.length-2);
      values.removeAt(values.length-1);
      if(listPostfix[i]=="+"){
        values.add((num1+num2).toString());
      }else if(listPostfix[i]=="-"){
        values.add((num1-num2).toString());
      }else if(listPostfix[i]=="*"){
        values.add((num1*num2).toString());
      }else if(listPostfix[i]=="/"){
        values.add((num1/num2).toInt().toString());
      }
    }
  }

  return values;
}